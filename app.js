var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var db = require('./mocks/database');
var Rooms = db.rooms;
var Users = db.users;
var Profiles = db.profiles;
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./mocks/database/scratch');
localStorage.setItem('lastTempValue', '15');
localStorage.setItem('lastHumValue', '50');

// var jwt = require('jsonwebtoken');
// var expressJwt = require('express-jwt');

var app = express();

// var jwtSecret = 'secretlySecret_donotlook!';
//
// var user = {
//   username: 'testuser',
//   password: 'testpass'
// };

app.use(favicon(path.join(__dirname, 'app/favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
//app.use(expressJwt({ secret: jwtSecret }).unless({ path: [ '/login', '/about' ]}));

app.disable('etag');
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

app.use('/bower_components', express.static(path.join(__dirname, '/bower_components')));
app.use(express.static(path.join(__dirname, '/app')));
//app.engine('html', require('ejs').renderFile);
//var router = require('./routes/index');
//app.use('/', router);



//var jwtCheck = expressJwt({
//  secret: jwtSecret
//});
//
//app.use('/api/', jwtCheck);


//Users.save({
//  username: 'admin',
//  password: 'admin',
//  //role: roles.admin,
//  rooms: [0, 1, 2]
//});
//
//Users.save({
//  username: 'user',
//  password: 'user',
//  //role: roles.user,
//  rooms: [2]
//});
//
//Users.save({
//  username: 'user2',
//  password: 'user2',
//  //role: roles.user,
//  rooms: [1]
//});
//
//function createToken(user) {
//  console.log("Token creation");
//  // var token = jwt.sign(user, jwtSecret, { expiresInMinutes: 60*5 });
//  // console.log("token = " + token);
//  var token = null;
//  return token;
//}

app.post('/auth/login', function (req, res) {
    var user = Users.findOne(req.body.username, req.body.password);

    var rights = false;
    if (user.username == "admin") {
        rights = true;
    }

    if (user) {
        // var token = createToken(user);
        res.status(201).json({
            type: true,
            data: user,
            // token: token,
            authorized: true,
            adminRights: rights
        });
    } else {
        res.status(401).json({
            type: false,
            data: "Incorrect email/password"
        });
    }
});

app.post('/auth/signup', function (req, res) {
    var user = Users.findOne(req.body.username, req.body.password);
    if (user) {
        res.json({
            type: false,
            data: "User already exists!"
        });
    } else {
        var userModel = { username: '', password: '', token: '' };
        userModel.username = req.body.username;
        userModel.password = req.body.password;
        Users.save();
        userModel.save(function (err, user) {
            //user.token = jwt.sign(user, process.env.JWT_SECRET);
            user.save(function (err, user1) {
                res.json({
                    type: true,
                    data: user1,
                    //token: user1.token,
                    authorized: true
                });
            });
        })
    }
});




app.get('/api', function (req, res) {
    res.send('');
});

app.get('/api/dashboard', function (req, res) {
    res.send('');
});

app.get('/charts/temperature', function (req, res) {
    var tempChange = 0;
    var lastTemp = parseFloat(localStorage.getItem('lastTempValue'));
    var currDate = new Date();
    if (currDate.getHours() > 16 && currDate.getHours() < 24 || currDate.getHours() > 24 && currDate.getHours() < 6) {
        if (lastTemp < 20)
            tempChange = lastTemp + 0.1;
    }
    else {
        if (lastTemp > 15)
            tempChange = lastTemp - 0.1;
    }
    localStorage.setItem('lastTempValue', '' + tempChange);

    res.json({ value: tempChange });
});

app.get('/charts/humidity', function (req, res) {
    var humChange = 0;
    var lastHum = parseFloat(localStorage.getItem('lastHumValue'));
    var currDate = new Date();
    if (currDate.getHours() > 16 && currDate.getHours() < 24 || currDate.getHours() > 24 && currDate.getHours() < 6) {
        if (lastHum > 30)
            humChange = lastHum - 0.1;
    }
    else {
        if (lastHum < 50)
            humChange = lastHum + 0.1;
    }
    localStorage.setItem('lastHumValue', '' + humChange);
    res.json({ value: humChange });
});

app.post('/dashboard/floor/:idFloor/room/:idRoom/relay/:idRelay', function (req, res) {
    var idFloor = req.params.idFloor;
    var idRoom = req.params.idRoom;
    var idRelay = req.params.idRelay;
    if (Rooms.checkIfExist(idFloor, idRoom, idRelay) != true)
        res.send("floor does not exists in database");
            return;
    var floor = Rooms.get(idFloor);

    floor.rooms[idRoom].relays[idRelay].active = !floor.rooms[idRoom].relays[idRelay].active;

    Rooms.update(floor);
    res.send('success');
});

app.get('/profiles/all', function (req, res) {
    // var profilesInfo = Profiles.getInitialInfo();
    var profilesInfo = Profiles.getAll();
    res.json({profiles: profilesInfo});
});

app.post('/profiles/heater/creation', function (req, res) {
    var newHeaterConf = req.body;
    Profiles.add(newHeaterConf);
    
    res.send('success');
});

app.post('/profiles/light/creation', function (req, res) {
    var newLightConf = req.body;
    Profiles.add(newLightConf);
    res.send('success');
});

app.post('/profiles/relay/creation', function (req, res) {
    var newRelayConf = req.body; 
    Profiles.add(newRelayConf);
    res.send('success');
});

app.get('/dashboard/floors', function (req, res) {
    var floors = Rooms.getAll();

    res.json({ msg: 'success!', data: floors });
});

app.get('/dashboard/lights', function (req, res) {
    var lights = Rooms.getAllLights();
    res.json({ msg: 'success!', data: lights });
});

app.get('/dashboard/relays', function (req, res) {
    var relays = Rooms.getAllRelays();
    res.json({ msg: 'success!', data: relays });
});

//app.post('/login', authenticate, function (req, res) {
//  var token = jwt.sign({
//    username: user.username
//  }, jwtSecret);
//  res.send({
//    token: token,
//    user: user
//  });
//});

//app.get('/api', function(request, response) {
//  console.log("/api")
//  response.render('index.html')
//});
//
//app.get('/api/dashboard', function(request, response) {
//  console.log("/api/dashboard")
//  response.render('/views/dashboard.html')
//});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        //res.render('error', {
        //  message: err.message,
        //  error: err
        //});
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    //res.render('error', {
    //  message: err.message,
    //  error: {}
    //});
});


module.exports = app;
