/**
 * The Index of Routes
 */

var express = require('express');
var router = express.Router();
var db = require('../mocks/database');
var Rooms = db.rooms;
var Users = db.users;

module.exports = function (/*passport*/) {
  //router.post('/auth/login', passport.authenticate('local-login',{
  //  successRedirect: '/#/profile',
  //  failureRedirect: '/#/login',
  //  failureFlash: true
  //})
  //);

  router.post('/auth/login', function(req, res) {
    console.log("try to auth");
      //res.json({
      //  type: false,
      //  data: "Incorrect email/password"
      //});
    res.send({ 'type': 'false', 'data': "Incorrect email/password"});
    res.end();
    //var user = Users.findOne(req.body.username, req.body.password);
    //
    //if (user) {
    //  res.json({
    //    type: true,
    //    data: user,
    //    token: user.token
    //  });
    //} else {
    //  res.json({
    //    type: false,
    //    data: "Incorrect email/password"
    //  });
    //}
  });

  router.post('/auth/signup', function(req, res) {
    var user = Users.findOne(req.body.username, req.body.password);
    if (user) {
      res.json({
        type: false,
        data: "User already exists!"
      });
    } else {
      var userModel = {username: '', password: '', token: ''};

      userModel.username = req.body.username;
      userModel.password = req.body.password;
      Users.save()
      userModel.save(function(err, user) {
        user.token = jwt.sign(user, process.env.JWT_SECRET);
        user.save(function(err, user1) {
          res.json({
            type: true,
            data: user1,
            token: user1.token
          });
        });
      })
    }
  });

  router.get('/api', function (req, res) {
    res.send('');
  });

  router.get('/api/dashboard', function (req, res) {
    res.send('');
  });

  router.get('/charts/temperature', function (req, res) {
    var max = 30;
    var min = 15;
    var temp = Math.random() * (max - min) + min;
    res.json({value: temp});
  });

  router.get('/charts/humidity', function (req, res) {
    var max = 0;
    var min = 100;
    var temp = Math.random() * (max - min) + min;
    res.json({value: temp});
  });

  router.post('/dashboard/room/:idRoom/relay/:idRelay', function (req, res) {
    var idRoom = req.params.idRoom;
    var idRelay = req.params.idRelay;
    var room = Rooms.get(idRoom);

    if (room === null) {
      res.send("room does not exists in database");
      return;
    }
    room.relays[idRelay].active = !room.relays[idRelay].active;

    Rooms.update(room);
    res.send('success');
  });

  router.get('/dashboard/rooms', function (req, res) {
    var rooms = Rooms.getAll();
    res.json({msg: 'success!', data: rooms});
  });

  router.get('/users', function (req, res, next) {
    res.send('respond with a resource');
  });

  return router;
};

function isLoggedIn(req, res, next) {

  // if user is authenticated in the session, carry on
  if (req.isAuthenticated())
    return next();

  // if they aren't redirect them to the home page
  res.redirect('/');
}
