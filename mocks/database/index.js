/**
 * Created by Aemulor on 2015-12-06.
 */
/**
 * Created by Aemulor on 2015-10-19.
 */
var Repository = require('./schemas/repository.js');
var RoomsRepository = require('./schemas/roomsRepo.js');
var UsersRepository = require('./schemas/userRepo.js');
var ProfilesRepository = require('./schemas/profilesRepo.js');

var userRepository = new UsersRepository();
userRepository.open();
console.log('user repository loaded');

// var sensorRepository = new Repository('mocks/files/sensors.txt');
// sensorRepository.open();
// console.log('sensor repository loaded');
// 
var profilesRepository = new ProfilesRepository();
profilesRepository.open();
console.log('profile repository loaded');

var roomsRepository = new RoomsRepository();
roomsRepository.open();
console.log('rooms repository loaded');

/*
 userRepository.save({
 username: 'admin',
 password: 'admin',
 //role: roles.admin,
 rooms: [0, 1, 2]
 });

 userRepository.save({
 username: 'user',
 password: 'user',
 //role: roles.user,
 rooms: [2]
 });

 userRepository.save({
 username: 'user2',
 password: 'user2',
 //role: roles.user,
 rooms: [1]
 });


 roomsRepository.save({
 name: 'Kitchen',
 mock: 'kitchen',
 relays: [
 {
 id: 0,
 name: 'Lamp',
 posX: 695,
 posY: 617,
 active: true
 },
 {
 id: 1,
 name: 'Fridge',
 posX: 448,
 posY: 801,
 active: true
 },
 {
 id: 2,
 name: 'Curtains',
 rect: [516, 818, 107, 15],
 active: true
 }
 ]
 });


 roomsRepository.save({
 name: 'Living room',
 mock: 'livingroom',
 relays: [{
 id: 0,
 name: 'Lamp',
 posX: 107,
 posY: 799,
 active: true
 },
 {
 id: 1,
 name: 'TV',
 posX: 107,
 posY: 577,
 active: true
 },
 {
 id: 2,
 name: 'Curtains',
 rect: [155, 818, 196, 15],
 active: true
 }
 ]
 });

 roomsRepository.save({
 name: 'Bedroom',
 mock: 'bedroom',
 relays: [
 {
 id: 0,
 name: 'Lamp',
 posX: 799,
 posY: 350,
 active: true
 },
 {
 id: 2,
 name: 'Curtains',
 rect: [956, 677, 172, 15],
 active: true
 }
 ]
 });
 */


exports.rooms = roomsRepository;
exports.users = userRepository;
exports.profiles = profilesRepository;


