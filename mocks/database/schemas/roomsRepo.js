/**
 * Created by Aemulor on 2015-11-15.
 */
var fs = require('fs');

function RoomsRepository() {
    this.mockFilePath = 'mocks/files/rooms.txt';
    this.floors = [];
}

RoomsRepository.prototype.newId = function () {
    var maxId = 0;
    if (this.floors.length == 0)
        return maxId;
    this.floors.forEach(function (floor) {
        if (maxId < floor.floor)
            maxId = floor.floor;
    });
    return maxId + 1;
}

RoomsRepository.prototype.saveMock = function () {
    var data = '';
    for (var i = 0; i < this.floors.length; i++) {
        data += JSON.stringify(this.floors[i]);
        if (i < this.floors.length - 1)
            data += '\n';
    }
    fs.closeSync(fs.openSync(this.mockFilePath, 'w'));
    fs.writeFileSync(this.mockFilePath, data);
}


RoomsRepository.prototype.find = function (whatToFind) {
    for (var i = 0; i < this.floors.length; i++) {
        if (this.floors[i].floor == whatToFind.floor) {
            return i;
        }
    }
    return -1;
}

RoomsRepository.prototype.findById = function (id) {
    for (var i = 0; i < this.floors.length; i++) {
        if (this.floors[i].floor == id) {
            return i;
        }
    }
    return -1;
}


RoomsRepository.prototype.open = function () {
    this.floors = [];
    try {
        var lines = fs.readFileSync(this.mockFilePath, 'utf8').split('\n');
        var rawData = '';
        for (var idx = 0; idx < lines.length; idx++) {
            this.floors.push(JSON.parse('' + lines[idx]));
        }
    } catch (e) {
        console.log("Exception caught! " + e);
    }
    console.log('RoomsRepository loaded');
}

RoomsRepository.prototype.close = function () {
    this.floors = [];
}

RoomsRepository.prototype.save = function (newElement) {
    if (this.find(newElement) == -1) {
        if (newElement.id == null) {
            newElement.id = this.newId();
        }
        this.floors.push(newElement);
        this.saveMock();
    }
}

RoomsRepository.prototype.update = function (elementToUpdate) {
    var idx = this.find(elementToUpdate);
    if (idx != -1) {
        this.floors[idx] = elementToUpdate;
        this.saveMock();
    }
}

RoomsRepository.prototype.getAll = function () {

    return this.floors;
}

RoomsRepository.prototype.getAllLights = function () {

    var lights = [];
    for (var i = 0; i < this.floors.length; i++) {
        for (var j = 0; j < this.floors[i].rooms.length; j++) {
            for (var k = 0; k < this.floors[i].rooms[j].relays.length; k++) {
                if (this.floors[i].rooms[j].relays[k].type == "light") {
                    var light = this.floors[i].rooms[j].relays[k];
                    light.floorId = i;
                    light.roomId = j;
                    light.relayId = k;
                    lights.push(light);
                }
            }
        }
    }
    //
    return lights;
}

RoomsRepository.prototype.getAllRelays = function () {

    var relays = [];
    for (var i = 0; i < this.floors.length; i++)
        for (var j = 0; j < this.floors[i].rooms.length; j++)
            for (var k = 0; k < this.floors[i].rooms[j].relays.length; k++)
                if (this.floors[i].rooms[j].relays[k].type != "light") {
                    var relay = this.floors[i].rooms[j].relays[k];
                    relay.floorId = i;
                    relay.roomId = j;
                    relay.relayId = k;
                    relays.push(relay);
                }
                    
    return relays;
}

RoomsRepository.prototype.checkIfExist = function (idFloor, idRoom, idRelay) {
    for (var i = 0; i < this.floors.length; i++)
        for (var j = 0; j < this.floors[i].rooms.length; j++)
            for (var k = 0; k < this.floors[i].rooms[j].relays.length; k++)
                if (this.floors[i].floor == idFloor && this.floors[i].rooms[j].id == idRoom && this.floors[i].rooms[j].relays[k].id == idRelay)
                    return true;
    return false;
}

RoomsRepository.prototype.get = function (id) {
    var idx = this.findById(id);
    if (idx != -1) {
        return this.floors[idx];
    }
    else
        return null;
}

module.exports = RoomsRepository;
