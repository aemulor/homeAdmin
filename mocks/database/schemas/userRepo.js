/**
 * Created by Aemulor on 2015-12-06.
 */
var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
/*
 {
 username: 'admin',
 password: 'admin',
 token: '',
 role: roles.admin,
 rooms: [0, 1, 2]
 }
 */

roles = ['admin', 'user', 'guest'];

function UsersRepository() {
    this.mockFilePath = './mocks/files/users.txt';
    this.users = [];
}

UsersRepository.prototype.newId = function () {
    var maxId = 0;
    if (this.users.length == 0)
        return maxId;
    this.users.forEach(function (user) {
        if (maxId < user.id)
            maxId = user.id;
    });
    return maxId + 1;
};

UsersRepository.prototype.saveMock = function () {
    var data = '';
    for (var i = 0; i < this.users.length; i++) {
        data += JSON.stringify(this.users[i]);
        if (i < this.users.length - 1)
            data += '\n';
    }
    fs.closeSync(fs.openSync(this.mockFilePath, 'w'));
    fs.writeFileSync(this.mockFilePath, data);
};


UsersRepository.prototype.find = function (whatToFind) {
    for (var i = 0; i < this.users.length; i++) {
        if (this.users[i].id == whatToFind.id || this.users[i].username == whatToFind.username) {
            return i;
        }
    }
    return -1;
};

UsersRepository.prototype.findById = function (id) {
    for (var i = 0; i < this.users.length; i++) {
        if (this.users[i].id == id) {
            return i;
        }
    }
    return -1;
};

UsersRepository.prototype.generateHash = function (password) {
    var salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
};

// checking if password is valid
UsersRepository.prototype.validPassword = function (password, localPassword) {

    var ret = bcrypt.compare(password, localPassword, function (err, result) {
    });

};


UsersRepository.prototype.findOne = function (username, password) {
    for (var i = 0; i < this.users.length; i++) {
        if (this.users[i].username == username && this.users[i].password == password) {
            return this.users[i];
        }
    }
    return null;
};


UsersRepository.prototype.open = function () {
    this.users = [];
    try {
        var lines = fs.readFileSync(this.mockFilePath, 'utf8').split('\n');
        var rawData = '';
        for (var idx = 0; idx < lines.length; idx++) {
            this.users.push(JSON.parse('' + lines[idx]));
        }
    } catch (e) {
        console.log("Exception caught! " + e);
    }
    console.log('UsersRepository loaded');
};

UsersRepository.prototype.close = function () {
    this.users = [];
};

UsersRepository.prototype.save = function (newElement) {
    if (this.find(newElement) == -1) {
        if (newElement.id == null) {
            newElement.id = this.newId();
        }
        this.users.push(newElement);
        this.saveMock();
    }
};

UsersRepository.prototype.update = function (elementToUpdate) {
    var idx = this.find(elementToUpdate);
    if (idx != -1) {
        this.users[idx] = elementToUpdate;
        this.saveMock();
    }
};

UsersRepository.prototype.getAll = function () {
    return this.users;
};

UsersRepository.prototype.get = function (id) {
    var idx = this.findById(id);
    if (idx != -1) {
        return this.users[idx];
    }
    else
        return null;
};

module.exports = UsersRepository;
