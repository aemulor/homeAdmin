/**
 * Created by Aemulor on 2015-11-15.
 */
var fs = require('fs');

function ProfilesRepository() {
    this.mockFilePath = 'mocks/files/profiles.txt';
    this.profiles = [];
}

ProfilesRepository.prototype.newId = function () {
    var maxId = 0;
    if (this.profiles.length == 0)
        return maxId;
    this.profiles.forEach(function (profile) {
        if (maxId < profile.id)
            maxId = profile.id;
    });
    return maxId + 1;
}

ProfilesRepository.prototype.saveMock = function () {
    var data = '';
    for (var i = 0; i < this.profiles.length; i++) {
        data += JSON.stringify(this.profiles[i]);
        if (i < this.profiles.length - 1)
            data += '\n';
    }
    fs.closeSync(fs.openSync(this.mockFilePath, 'w'));
    fs.writeFileSync(this.mockFilePath, data);
}


ProfilesRepository.prototype.find = function (whatToFind) {
    for (var i = 0; i < this.profiles.length; i++) {
        if (this.profiles[i].id !== undefined && this.profiles[i].id == whatToFind.id) {
            return i;
        }
    }
    return -1;
}

ProfilesRepository.prototype.findById = function (id) {
    for (var i = 0; i < this.profiles.length; i++) {
        if (this.profiles[i].id == id) {
            return i;
        }
    }
    return -1;
}


ProfilesRepository.prototype.open = function () {
    this.profiles = [];
    try {
        var lines = fs.readFileSync(this.mockFilePath, 'utf8').split('\n');
        var rawData = '';
        for (var idx = 0; idx < lines.length; idx++) {
            this.profiles.push(JSON.parse('' + lines[idx]));
        }
    } catch (e) {
        console.log("Exception caught! " + e);
    }
    console.log('ProfilesRepository loaded');
}

ProfilesRepository.prototype.close = function () {
    this.profiles = [];
}

ProfilesRepository.prototype.add = function (newElement) {
       if (newElement.id == null) {
            newElement.id = this.newId();
        }
        this.profiles.push(newElement);
        this.saveMock();
}

ProfilesRepository.prototype.save = function (newElement) {
    if (this.find(newElement) == -1) {
        if (newElement.id == null) {
            newElement.id = this.newId();
        }
        this.profiles.push(newElement);
        this.saveMock();
    }
}

ProfilesRepository.prototype.update = function (elementToUpdate) {
    var idx = this.find(elementToUpdate);
    if (idx != -1) {
        this.profiles[idx] = elementToUpdate;
        this.saveMock();
    }
}

ProfilesRepository.prototype.getAll = function () {
    return this.profiles;
}

ProfilesRepository.prototype.getInitialInfo = function () {
    var returnData = [];
     for (var i = 0; i < this.profiles.length; i++) {
         var profile = {id: 0, name: '', desc: '', type: ''};
         profile.id = this.profiles[i].id;
         profile.name = this.profiles[i].name;
         profile.desc = this.profiles[i].desc;
         profile.type = this.profiles[i].type;
         returnData.push(profile);
     }
    
    return returnData;
}

ProfilesRepository.prototype.get = function (id) {
    var idx = this.findById(id);
    if (idx != -1) {
        return this.profiles[idx];
    }
    else
        return null;
}

module.exports = ProfilesRepository;
