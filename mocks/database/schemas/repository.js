/**
 * Created by Aemulor on 2015-10-19.
 */
var fs = require('fs');

var roles = [];

function Repository(filepath) {
    this.mockFilePath = filepath;
    this.elements = [];
}

Repository.prototype.newId = function () {
    var maxId = 0;
    if (this.elements.length == 0)
        return maxId;
    this.elements.forEach(function (el) {
        if (maxId < el.id)
            maxId = el.id;
    });
    return maxId + 1;
}

Repository.prototype.saveMock = function () {
    var data = '';
    for (var i = 0; i < this.elements.length; i++) {
        data += JSON.stringify(this.elements[i]);
        if (i < this.elements.length - 1)
            data += '\n';
    }
    fs.closeSync(fs.openSync(this.mockFilePath, 'w'));
    fs.writeFileSync(this.mockFilePath, data);
}


Repository.prototype.find = function (whatToFind) {
    for (var i = 0; i < this.elements.length; i++) {
        if (this.elements[i].id == whatToFind.id) {
            return i;
        }
    }
    return -1;
}

Repository.prototype.findById = function (id) {
    for (var i = 0; i < this.elements.length; i++) {
        if (this.elements[i].id == id) {
            return i;
        }
    }
    return -1;
}


Repository.prototype.findOne = function (whatToFind) {
    for (var i = 0; i < this.elements.length; i++) {
        if (this.elements[i].id == whatToFind.id) {
            return i;
        }
    }
    return -1;
}


Repository.prototype.open = function () {
    this.elements = [];
    try {
        var lines = fs.readFileSync(this.mockFilePath, 'utf8').split('\n');
        var rawData = '';
        for (var idx = 0; idx < lines.length; idx++) {
            this.elements.push(JSON.parse('' + lines[idx]));
        }
    } catch (e) {
        console.log("Exception caught! " + e);
    }
    console.log('Repository loaded');
}

Repository.prototype.close = function () {
    this.elements = [];
}

Repository.prototype.save = function (newElement) {
    if (this.find(newElement) == -1) {
        if (newElement.id == null) {
            newElement.id = this.newId();
        }
        this.elements.push(newElement);
        this.saveMock();
    }
}

Repository.prototype.update = function (elementToUpdate) {
    var idx = this.find(elementToUpdate);
    if (idx != -1) {
        this.elements[idx] = elementToUpdate;
        this.saveMock();
    }
}

Repository.prototype.getAll = function () {
    return this.elements;
}

Repository.prototype.get = function (id) {
    var idx = this.findById(id);
    if (idx != -1) {
        return this.elements[idx];
    }
    else
        return null;
}

module.exports = Repository;
