'use strict';

/**
 * @ngdoc overview
 * @name clientApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 */
var app = angular
  .module('clientApp', [
    'oc.lazyLoad',
    'ui.router',
    'ngStorage',
     'angular-loading-bar',
     'ui.bootstrap',
     'smart-table',
     'frapontillo.bootstrap-switch'
  ])
.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = true;
}])
  .config(['$urlRouterProvider', '$stateProvider', '$httpProvider', '$ocLazyLoadProvider', function ($urlRouterProvider, $stateProvider, $httpProvider, $ocLazyLoadProvider) {
     $ocLazyLoadProvider.config({
       debug: false,
       events: true
     });
    //$urlRouterProvider.otherwise('/login');
    $urlRouterProvider.otherwise('/api/profiles');
    // jwtInterceptorProvider.tokenGetter = function(store) {
    //   return store.get('jwt');
    // };
    // $httpProvider.interceptors.push('jwtInterceptor');
    //$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $stateProvider
      //.state('about', {
      //  url: '/about',
      //  controller: 'AboutCtrl',
      //  templateUrl: 'views/about.html',
      //  data: {
      //    requiresLogin: false
      //  }
      //})
      .state('login', {
        url: '/login',
        controller: 'LoginCtrl',
        templateUrl: 'views/login.html',
        data: {
          requiresLogin: false
        },
        resolve: {
          loadMyFile: function($ocLazyLoad){
            return $ocLazyLoad.load({
              name: 'clientApp',
              files: ['scripts/controllers/login.js']
            }),
              $ocLazyLoad.load({
                name: 'ngStorage',
                files:['bower_components/ngstorage/ngStorage.js']
              }),
              $ocLazyLoad.load({
               name:'ngResource',
               files:['bower_components/angular-resource/angular-resource.js']
              })
          }}
      })
      .state('api', {
        url: '/api',
        templateUrl: 'views/main.html',
        data: {
          requiresLogin: true
        },
        resolve: {
          loadMyDirectives:function($ocLazyLoad){
            return $ocLazyLoad.load({
                name:'clientApp',
                files:['directives/header/header.js']
              }),
              $ocLazyLoad.load({
                  name:'jquery',
                  files:['bower_components/jquery/dist/jquery.js']
                }),
              $ocLazyLoad.load({
                  name:'ngAnimate',
                  files:['bower_components/angular-animate/angular-animate.js']
                }),
              $ocLazyLoad.load({
                name:'ngResource',
                files:['bower_components/angular-resource/angular-resource.js']
              }),
              $ocLazyLoad.load({
                name:'ngSanitize',
                files:['bower_components/angular-sanitize/angular-sanitize.js']
              }),
              $ocLazyLoad.load(
              {
                name:'ngTouch',
                files:['bower_components/angular-touch/angular-touch.js']
              }),
              $ocLazyLoad.load(
                {
                  name:'ngStorage',
                  files:['bower_components/ngstorage/ngStorage.js']
                }),
              $ocLazyLoad.load(
                {
                  name:'ngCookies',
                  files:['bower_components/angular-cookies/angular-cookies.js']
                }),
              $ocLazyLoad.load(
                {
                  name:'ngMessages',
                  files:['bower_components/angular-messages/angular-messages.js']
                }),
              $ocLazyLoad.load(
                {
                  name:'ngAria',
                  files:['bower_components/angular-aria/angular-aria.js']
                })

          }
        }
      })
      .state('api.home', {
        url: '/home',
        templateUrl: 'views/home.html'

      })
      .state('api.dashboard', {
        url: '/dashboard',
        controller: 'DashboardCtrl',
        templateUrl: 'views/dashboard.html',
        resolve: {
          loadMyFile: function($ocLazyLoad){
            return $ocLazyLoad.load({
              name: 'clientApp',
              files: ['scripts/controllers/dashboard.js']
            })
          }}

      })
      .state('api.charts', {
        url: '/charts',
        controller: 'ChartsCtrl',
        templateUrl: 'views/charts.html',
        resolve: {
          loadMyFile: function($ocLazyLoad){
            return $ocLazyLoad.load({
              name: 'clientApp',
              files: ['scripts/controllers/charts.js']
            }),
              $ocLazyLoad.load({
                name: 'highcharts-ng',
                files: ['bower_components/highcharts-ng/dist/highcharts-ng.js']
              })

          }}
      })
      .state('api.sensor_view', {
        url: '/sensor_view',
        controller: 'SensorViewCtrl',
        templateUrl: 'views/sensor_view.html',
        resolve: {
          loadMyFile: function($ocLazyLoad){
            return $ocLazyLoad.load({
              name: 'clientApp',
              files: ['scripts/controllers/sensor_view.js']
            })
          }}
      })
      .state('api.profiles', {
        url: '/profiles',
        controller: 'ProfilesCtrl',
        templateUrl: 'views/profiles.html',
        resolve: {
          loadMyFile: function($ocLazyLoad){
            return $ocLazyLoad.load({
                name: 'clientApp',
                files: [
                  'directives/stats/stats.js',
                  'directives/heaterConf/heaterConf.js',
                  'directives/heaterConf/heaterNode/heaterNode.js',
                  'directives/lightConf/lightConf.js',
                  'directives/lightConf/lightNode/lightNode.js',
                  'directives/relayConf/relayConf.js',
                  'directives/relayConf/relayNode/relayNode.js',
                  'scripts/controllers/profiles.js']
              })           
        }}
      })
      .state('api.settings', {
        url: '/settings',
        controller: 'SettingsCtrl',
        templateUrl: 'views/settings.html',
        resolve: {
          loadMyFile: function($ocLazyLoad){
            return $ocLazyLoad.load({
              name: 'clientApp',
              files: ['scripts/controllers/settings.js']
            })
          }}

      })
      .state('api.logout', {
        url: '/logout',
        controller: 'LogoutCtrl',
        resolve: {
          loadMyFile: function($ocLazyLoad){
            return $ocLazyLoad.load({
              name: 'clientApp',
              files: ['scripts/controllers/logout.js']
            })
          }}
      });
  }])
//.run(['$rootScope', '$state', 'store', 'jwtHelper', function($rootScope, $state, store, jwtHelper) {
  .run(['$rootScope', '$state', '$sessionStorage', function ($rootScope, $state, $sessionStorage) {
    $rootScope.$on("$stateChangeStart", function (e, toState, toParams, fromState, fromParams) {
      
      if (toState.data && toState.data.requiresLogin) {
        if ($sessionStorage.user == null || $sessionStorage.user.authorized == false)
        {
            //$state.go('login');
            $state.go('api.profiles');
        }
      }
      console.log(toState.name);
    });
  }]);
