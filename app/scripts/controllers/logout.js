/**
 * Created by Aemulor on 2015-11-14.
 */
'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('LogoutCtrl', ['$rootScope', '$scope', '$http', '$state', '$sessionStorage', '$localStorage', function ($rootScope, $scope, $http, $state, $sessionStorage, $localStorage) {
    $sessionStorage.$reset();
    $localStorage.$reset();
    $state.go('login');
  }]);
