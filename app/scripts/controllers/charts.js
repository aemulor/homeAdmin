'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ChartsCtrl
 * @description
 * # ChartsCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
    .controller('ChartsCtrl', ['$scope', '$state', '$http', '$localStorage', '$sessionStorage', function ($scope, $state, $http, $localStorage, $sessionStorage) {
        $scope.refreshRate = $localStorage.refreshRate || 10;
        $scope.tempData = $localStorage.tempData || [[Date.now(), 15]];
        $scope.humData = $localStorage.humData || [[Date.now(), 50]];
        $scope.heatingStarted = false;
        $scope.heatingStopped = true;
        $scope.tempChartConfig = {
            options: {
                chart: {
                    type: 'line',
                    zoomType: 'y'
                }
            },
            series: [{
                name: '',
                data: $scope.tempData
            }],
            title: {
                text: 'Temperature'
            },
            xAxis: {
                type: 'datetime',
            },
            loading: false
        };

        $scope.humChartConfig = {
            options: {
                chart: {
                    type: 'line',
                    zoomType: 'x'
                }
            },
            series: [{
                name: '',
                data: $scope.humData
            }],
            title: {
                text: 'Humidity'
            },
            xAxis: {
                type: 'datetime',
            },

            loading: false
        };

        $scope.$watch("refreshRate", function (newValue, oldValue) {
            $localStorage.refreshRate = $scope.refreshRate;
        });
        if ($sessionStorage.loggedUser != undefined) {
            $scope.getData = function () {


                var currDate = new Date();
                if (currDate.getHours() > 16 && currDate.getHours() < 24 || currDate.getHours() > 24 && currDate.getHours() < 6) {
                    $scope.heatingStarted = true;
                    $scope.heatingStarted = false;
                }
                else {
                    $scope.heatingStarted = false;
                    $scope.heatingStopped = true;
                }
                $http.get('/charts/temperature')
                    .success(function (res) {
                        if ($scope.tempData.length >= 10)
                            $scope.tempData.splice(0, 1);
                        $scope.tempData.push([Date.now(), parseFloat(res.value)]);
                        $localStorage.tempData = $scope.tempData;
                    });
                $http.get('/charts/humidity')
                    .success(function (res) {
                        if ($scope.humData.length >= 10)
                            $scope.humData.splice(0, 1);
                        $scope.humData.push([Date.now(), parseFloat(res.value)]);
                        $localStorage.humData = $scope.humData;
                    });
            };
            setInterval($scope.getData, $scope.refreshRate * 1000);
        }

    }]);
