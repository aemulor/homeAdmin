'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ProfilesCtrl
 * @description
 * # ProfilesCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
    .controller('ProfilesCtrl', ['$scope', '$rootScope','$state', '$http', function ($scope, $rootScope, $state, $http) {
        $scope.type = "displayAll";
        $scope.edit = false;
        // $scope.selectedProfile = undefined;
        $scope.profilesInfo = {};        
        
        $http.get('/profiles/all').success(function (res) {
            $scope.profilesInfo = res.profiles;
            
        });
        
        $scope.viewDetailsAbout = function(prf) {
            console.log("$scope.viewDetailsAbout");
            // console.log(JSON.stringify(prf));
            if (prf.type == 'heater') {
                // $scope.editHeaterProfile(prf);
                $rootScope.$broadcast('editHeaterProfile', prf);
                $scope.type = "heaterProfileCreator";
            } else if (prf.type == 'light') {
                // $scope.editLightProfile(prf);
                $rootScope.$broadcast('editLightProfile', prf);
                $scope.type = "lightProfileCreator";
            } else if (prf.type == 'relay') {
                // $scope.editRelayProfile(prf);
                $rootScope.$broadcast('editRelayProfile', prf);
                $scope.type = "relayProfileCreator";
            }
        }

        $scope.addNewHeaterProfile = function () {
            if ($scope.type == "heaterProfileCreator")
                $scope.type = "displayAll";
            else
                $scope.type = "heaterProfileCreator";
        }

        $scope.addNewLightProfile = function () {
            if ($scope.type == "lightProfileCreator")
                $scope.type = "displayAll";
            else
                $scope.type = "lightProfileCreator";
        }

        $scope.addNewRelayProfile = function () {
            if ($scope.type == "relayProfileCreator")
                $scope.type = "displayAll";
            else
                $scope.type = "relayProfileCreator";
        }

    }]);
