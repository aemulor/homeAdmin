/**
 * Created by Aemulor on 2015-10-21.
 */
angular.module('clientApp')
  .controller('NavigationCtrl', ['$scope', '$rootScope', '$location', function ($scope, $rootScope, $location) {
    $scope.isActive = function (path) {
      return $location.path() == path;
    };

    $rootScope.isLoggedIn = false;

  }]);
