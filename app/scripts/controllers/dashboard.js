'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
    .controller('DashboardCtrl', ['$scope', '$state', '$http', function ($scope, $state, $http) {
        
        var request = $http.get('/dashboard/floors');      
        request.success(function (res) {
            $scope.floors = $scope.floors || res.data;
            $scope.isAllCurtainToggle = isAnyCurtainOpen();
            $scope.isPowerOn = isAnyPowerOn();            
        });

        function isAnyCurtainOpen() {
            for (var i = 0; i < $scope.floors.length; i++) {
                for (var j = 0; j < $scope.floors[i].rooms.length; j++) {
                    for (var k = 0; k < $scope.floors[i].rooms[j].relays.length; k++) {
                        if (containsStr($scope.floors[i].rooms[j].relays[k].type, "curtains") && $scope.floors[i].rooms[j].relays[k].active) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }


        function isAnyPowerOn() {
            for (var i = 0; i < $scope.floors.length; i++) {
                for (var j = 0; j < $scope.floors[i].rooms.length; j++) {
                    for (var k = 0; k < $scope.floors[i].rooms[j].relays.length; k++) {
                        if (!containsStr($scope.floors[i].rooms[j].relays[k].type, "curtains") && $scope.floors[i].rooms[j].relays[k].active) {
                            return true;
                        }
                    }
                }
            }
            return false;
        };

        $scope.closeAllCurtains = function (event) {
            var btnState = event.target.attributes.class.value == "btn btn-success" ? true : false;
            for (var i = 0; i < $scope.floors.length; i++) {
                for (var j = 0; j < $scope.floors[i].rooms.length; j++) {
                    for (var k = 0; k < $scope.floors[i].rooms[j].relays.length; k++) {
                        if ($scope.floors[i].rooms[j].relays[k].type == "curtain") {
                            $scope.floors[i].rooms[j].relays[k].active = !btnState;
                            var floorId = $scope.floors[i].floor;
                            var roomId = $scope.floors[i].rooms[j].id;
                            var relayId = $scope.floors[i].rooms[j].relays[k].id;
                            $http.post('/dashboard/floor/' + floorId + '/room/' + roomId + '/relay/' + relayId, $scope.floors[i]);
                        }
                    }
                }
            }

            if (event.target.attributes.class.value == "btn btn-success")
                event.target.attributes.class.value = "btn btn-danger";
            else
                event.target.attributes.class.value = "btn btn-success";
        };

        $scope.onTogglePowerClicked = function (event) {
            var btnState = event.target.attributes.class.value == "btn btn-success" ? true : false;
            for (var i = 0; i < $scope.floors.length; i++) {
                for (var j = 0; j < $scope.floors[i].rooms.length; j++) {
                    for (var k = 0; k < $scope.floors[i].rooms[j].relays.length; k++) {
                        if ($scope.floors[i].rooms[j].relays[k].type != "curtain") {
                            $scope.floors[i].rooms[j].relays[k].active = !btnState;
                            var floorId = $scope.floors[i].floor;
                            var roomId = $scope.floors[i].rooms[j].id;
                            var relayId = $scope.floors[i].rooms[j].relays[k].id;
                            $http.post('/dashboard/floor/' + floorId + '/room/' + roomId + '/relay/' + relayId, $scope.floors[i]);
                        }
                    }
                }
            }
            if (event.target.attributes.class.value == "btn btn-success")
                event.target.attributes.class.value = "btn btn-danger";
            else
                event.target.attributes.class.value = "btn btn-success";
        };

        $scope.toggleStatus = function (floorId, roomId, relayId) {
                for (var j = 0; j < $scope.floors[floorId].rooms.length; j++) {
                    for (var k = 0; k < $scope.floors[floorId].rooms[j].relays.length; k++) {
                        if ($scope.floors[floorId].rooms[j].id == roomId && $scope.floors[floorId].rooms[j].relays[k].id == relayId) {
                            $scope.floors[floorId].rooms[j].relays[k].active = !$scope.floors[floorId].rooms[j].relays[k].active;
                            $http.post('/dashboard/floor/' + floorId + '/room/' + roomId + '/relay/' + relayId, $scope.floors[floorId]);
                        }
                    }
                }
            //$scope.floors[floorId].rooms[roomId].relays[relayId].active = !$scope.floors[floorId].rooms[roomId].relays[relayId].active;            
        };

        function containsStr(text, str) {
            if (text.toLowerCase().indexOf(str) > -1)
                return true;
            else
                return false;
        
        }
        

    }]);
