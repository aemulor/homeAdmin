'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:SensorViewCtrl
 * @description
 * # SensorViewCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
    .controller('SensorViewCtrl', ['$scope', '$state', '$http', function ($scope, $state, $http) {
        var canvas = document.getElementById("canvas");
        var context = canvas.getContext('2d');
        $scope.floors = null;
        $scope.displayInfo = false;
        $scope.relayInfo = { name: "", totalUptime: 0, onFrom: new Date(), offFrom: new Date(), lastOn: new Date(), inTheSystemFrom: new Date() };

        var init = function () {
            redraw();
        }
        init();

        function loadImages(paths, whenLoaded) {
            var imgs = [];
            paths.forEach(function (path) {
                var img = new Image();
                img.onload = function () {
                    imgs.push(img);
                    if (imgs.length == paths.length) whenLoaded(imgs);
                }
                img.src = path.src;
                img.posX = path.posX;
                img.posY = path.posY;
            });
        }

        function redraw() {
            $http.get('/dashboard/floors').success(function (res) {
                $scope.floors = $scope.floors || res.data;
                var relayImages = [];
                var img = new Image();
                img.onload = function () {
                    canvas.width = img.width;
                    canvas.height = img.height;
                    context.clearRect(0, 0, canvas.width, canvas.height);
                    context.drawImage(img, 0, 0);
                    for (var i = 0; i < $scope.floors[0].rooms.length; ++i) {
                        for (var j = 0; j < $scope.floors[0].rooms[i].relays.length; j++) {
                            var relayType = $scope.floors[0].rooms[i].relays[j].type;
                            if (relayType == "light") {
                                var relayImg = {}
                                relayImg.src = $scope.floors[0].rooms[i].relays[j].active == true ? "mocks/relays/light_on.png" : "mocks/relays/light_off.png";
                                relayImg.posX = $scope.floors[0].rooms[i].relays[j].posX;
                                relayImg.posY = $scope.floors[0].rooms[i].relays[j].posY;
                                relayImages.push(relayImg);
                            } else if (relayType == "tv") {
                                var relayImg = {};
                                relayImg.src = $scope.floors[0].rooms[i].relays[j].active == true ? "mocks/relays/tv_on.png" : "mocks/relays/tv_off.png";
                                relayImg.posX = $scope.floors[0].rooms[i].relays[j].posX;
                                relayImg.posY = $scope.floors[0].rooms[i].relays[j].posY;
                                relayImages.push(relayImg);
                            } else if (relayType == "radio") {
                                var relayImg = {};
                                relayImg.src = $scope.floors[0].rooms[i].relays[j].active == true ? "mocks/relays/radio_on.png" : "mocks/relays/radio_off.png";
                                relayImg.posX = $scope.floors[0].rooms[i].relays[j].posX;
                                relayImg.posY = $scope.floors[0].rooms[i].relays[j].posY;
                                relayImages.push(relayImg);
                            } else if (relayType == "camera") {
                                var relayImg = {};
                                relayImg.src = $scope.floors[0].rooms[i].relays[j].active == true ? "mocks/relays/camera_on.png" : "mocks/relays/camera_off.png";
                                relayImg.posX = $scope.floors[0].rooms[i].relays[j].posX;
                                relayImg.posY = $scope.floors[0].rooms[i].relays[j].posY;
                                relayImages.push(relayImg);
                            } else if (relayType == "curtain") {
                                var r = $scope.floors[0].rooms[i].relays[j].rect;
                                //living room window:
                                context.beginPath();
                                context.rect(r[0], r[1], r[2], r[3]);
                                context.fillStyle = $scope.floors[0].rooms[i].relays[j].active == true ? 'green' : 'red';
                                context.fill();
                                context.lineWidth = 5;
                                context.strokeStyle = 'black';
                                context.stroke();
                            } else if (relayType == "alarm") {
                                var relayImg = {};
                                relayImg.src = $scope.floors[0].rooms[i].relays[j].active == true ? "mocks/relays/alarm_on.png" : "mocks/relays/alarm_off.png";
                                relayImg.posX = $scope.floors[0].rooms[i].relays[j].posX;
                                relayImg.posY = $scope.floors[0].rooms[i].relays[j].posY;
                                relayImages.push(relayImg);
                            }
                        }
                    }

                    loadImages(relayImages, function (imgs) {
                        imgs.forEach(function (img) {
                            context.drawImage(img, img.posX, img.posY, 32, 32);
                        });
                    });
                };



                img.src = 'mocks/plan.png';
            });
        }

        // Add event listener for `click` events.
        canvas.addEventListener('dblclick', function (event) {
            var x = event.pageX - canvas.offsetLeft,
                y = event.pageY - canvas.offsetTop;
            var shouldRedraw = false;

            for (var i = 0; i < $scope.floors[0].rooms.length; ++i) {
                for (var j = 0; j < $scope.floors[0].rooms[i].relays.length; ++j) {
                    var element = $scope.floors[0].rooms[i].relays[j];
                    if (element.rect != undefined) {
                        if (y > element.rect[1] && y < element.rect[1] + element.rect[3]
                            && x > element.rect[0] && x < element.rect[0] + element.rect[2]) {
                            $scope.floors[0].rooms[i].relays[j].active = !$scope.floors[0].rooms[i].relays[j].active;
                            $http.post('/dashboard/floor/0/room/' + $scope.floors[0].rooms[i].id + '/relay/' + $scope.floors[0].rooms[i].relays[j].id, $scope.floors[0].rooms[i]);
                            shouldRedraw = true;
                        }
                    } else {
                        if (y > element.posY && y < element.posY + 32
                            && x > element.posX && x < element.posX + 32) {
                            $scope.floors[0].rooms[i].relays[j].active = !$scope.floors[0].rooms[i].relays[j].active;
                            $http.post('/dashboard/floor/0/room/' + $scope.floors[0].rooms[i].id + '/relay/' + $scope.floors[0].rooms[i].relays[j].id, $scope.floors[0].rooms[i]);
                            shouldRedraw = true;
                        }
                    }

                }
            }
            if (shouldRedraw) {
                redraw();
            }

        }, false);




    }]);
