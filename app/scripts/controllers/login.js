/**
 * Created by Aemulor on 2015-11-14.
 */
'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
    .controller('LoginCtrl', ['$rootScope', '$scope', '$http', '$state', '$sessionStorage', '$localStorage', function ($rootScope, $scope, $http, $state, $sessionStorage, $localStorage) {
        $scope.user = { username: "", password: "" };
        $scope.rememberme = false;
        $scope.alert = '';

        if ($localStorage.user != null) {
            $scope.user.username = $localStorage.user.username;
            $scope.user.password = $localStorage.user.password;
            $scope.rememberme = $localStorage.user.rememberme;
        }

        $scope.tryToLogin = function () {
            console.log('try to login ' + JSON.stringify($scope.user));


            $http.post('http://localhost:3000/auth/login', $scope.user)
                .success(function (response) {
                    // store.set('jwt', response.data.token);
                    // store.set('authenticated', response.data.authenticated);
                    if (response.authorized === true) {
                        //$rootScope.isLoggedIn = true;
                        var loggedUser = { username: $scope.user.username, authorized: response.authorized, isAdmin: response.adminRights, password: "", rememberme: $scope.rememberme};
                        $sessionStorage.user = loggedUser;
                        
                        if ($scope.rememberme) {
                            loggedUser.password = $scope.user.password;
                            $localStorage.user = loggedUser;
                        }
                        $state.go('api.home');
                    }
                    else {
                        $sessionStorage.$reset();
                        $localStorage.$reset();
                        $scope.alert = "Password or username is invalid.";
                        $state.go('login');
                    }

                })
                .error(function (error) {
                    if (error)
                        console.log(error.data);
                });
        }
    }]);
