angular.module('clientApp')
    .directive('relayConf', ['$http', '$compile', function ($http, $compile) {
        return {
            templateUrl: 'directives/relayConf/relayConf.html',
            restrict: 'AE',
            replace: true,
            scope: {
                profile: "=",
                shouldShow: "="
            },
            controller: function ($scope, $http, $compile) {
                var request = $http.get('/dashboard/relays');
                request.success(function (res) {
                    $scope.relays = $scope.relays || res.data;

                });

                $scope.relayConf = {
                    type: "relay",
                    name: "",
                    desc: "",
                    startDate: new Date(),
                    endDate: new Date(),
                    settings: []
                }

                $scope.$on('editRelayProfile', function (event, data) {
                    $scope.editRelayProfile(data.prf);
                });

                $scope.editRelayProfile = function (profile) {
                    $scope.relayConf = profile;
                    $scope.relayConf.startDate = new Date($scope.relayConf.startDate);
                    $scope.relayConf.endDate = new Date($scope.relayConf.endDate);

                    for (var i = 0; i < $scope.relayConf.settings.lenght; ++i) {
                        $scope.relayConf.settings.start = new Date($scope.relayConf.settings.start);
                        $scope.relayConf.settings.end = new Date($scope.relayConf.settings.end);
                    }
                }

                $scope.addRow = function () {
                    var dummyProfile = {
                        id: $scope.relayConf.settings.length,
                        days: {
                            monday: false,
                            tuesday: true,
                            wednesday: false,
                            thursday: false,
                            friday: false,
                            saturday: false,
                            sunday: false
                        },
                        start: new Date(),
                        end: new Date(),
                        relays: $scope.relays,
                        isActive: false
                    };

                    $scope.relayConf.settings.push(dummyProfile);
                };

                $scope.removeRow = function () {
                    $scope.relayConf.settings.pop();
                };

                $scope.repeatEveryWeek = false;
                $scope.calDateStart = new Date();
                $scope.calDateEnd = new Date();

                $scope.today = function () {
                    $scope.calDateStart = new Date();
                    $scope.calDateEnd = new Date();
                };
                $scope.today();

                $scope.clear = function () {
                    $scope.calDateStart = null;
                    $scope.calDateEnd = null;
                };

                $scope.disabled = function (date, mode) {
                    return '';
                };

                $scope.toggleMin = function () {
                    $scope.minDate = $scope.minDate ? null : new Date();
                };

                $scope.toggleMin();

                $scope.openStart = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.openedS = true;
                    $scope.openedE = false;
                };

                $scope.openEnd = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.openedS = false;
                    $scope.openedE = true;
                };

                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };

                $scope.formats = ['baseDate', 'yyyy-MM-dd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];

                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                var afterTomorrow = new Date();
                afterTomorrow.setDate(tomorrow.getDate() + 2);
                $scope.events = [{
                    date: tomorrow,
                    status: 'full'
                }, {
                        date: afterTomorrow,
                        status: 'partially'
                    }];

                $scope.getDayClass = function (date, mode) {
                    if (mode === 'day') {
                        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                        for (var i = 0; i < $scope.events.length; i++) {
                            var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                            if (dayToCheck === currentDay) {
                                return $scope.events[i].status;
                            }
                        }
                    }

                    return '';
                };

                $scope.saveProfile = function () {
                    $http.post('/profiles/light/creation', $scope.relayConf);
                    $scope.shouldShow = "displayAll";

                }

                $scope.cancelProfile = function () {
                    $scope.shouldShow = "displayAll";
                }
            }
        }
    }]);
        