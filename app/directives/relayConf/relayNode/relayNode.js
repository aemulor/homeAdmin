'use strict';

angular.module('clientApp')
    .directive('relayNode', ['$http',
        function ($http) {
            return {
                restrict: 'AE',
                scope: {
                    id: "=",
                    days: "=",
                    start: "=",
                    end: "=",
                    relays: "=",
                    isActive: "="
                },
                templateUrl: 'directives/relayConf/relayNode/relayNode.html',
                controller: function ($scope) {

                },
                link: function (scope, element, attrs) {
                }
            };
        }]);
