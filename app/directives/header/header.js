angular.module('clientApp')
  .directive('header', ['$location', function() {
    return {
      templateUrl:'directives/header/header.html',
      restrict: 'E',
      replace: true,
      scope: {
      },
      controller:function($scope)
      {
      }
    }
  }]);
