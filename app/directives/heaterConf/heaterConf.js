angular.module('clientApp')
    .directive('heaterConf', ['$http', '$compile', function ($http, $compile) {
        return {
            templateUrl: 'directives/heaterConf/heaterConf.html',
            restrict: 'AE',
            replace: true,
            scope: {
                profile: "=",
                shouldShow: "="
            },

            controller: function ($scope, $compile) {
                $scope.heaterConf = {
                    type: "heater",
                    name: "",
                    desc: "",
                    startDate: new Date(),
                    endDate: new Date(),
                    settings: [{
                        id: 0,
                        days: {
                            monday: false,
                            tuesday: true,
                            wednesday: false,
                            thursday: false,
                            friday: false,
                            saturday: false,
                            sunday: false
                        },
                        start: new Date(),
                        end: new Date(),
                        avgTemp: 15
                    }]
                }

                $scope.$on('editHeaterProfile', function (event, data) {
                    $scope.editHeaterProfile(data);
                });

                $scope.editHeaterProfile = function (profile) {
                    //console.log("editHeaterProfile!");
                    //console.log(JSON.stringify(profile));

                    $scope.heaterConf = profile;
                    $scope.heaterConf.startDate = new Date($scope.heaterConf.startDate);
                    $scope.heaterConf.endDate = new Date($scope.heaterConf.endDate);

                    for (var i = 0; i < $scope.heaterConf.settings.lenght; ++i) {
                        $scope.heaterConf.settings.start = new Date($scope.heaterConf.settings.start);
                        $scope.heaterConf.settings.end = new Date($scope.heaterConf.settings.end);
                    }
                }


                $scope.addRow = function () {
                    var dummyProfile = {
                        id: $scope.heaterConf.settings.length,
                        days: {
                            monday: false,
                            tuesday: true,
                            wednesday: false,
                            thursday: false,
                            friday: false,
                            saturday: false,
                            sunday: false
                        },
                        start: new Date(),
                        end: new Date(),
                        avgTemp: 15
                    };

                    $scope.heaterConf.settings.push(dummyProfile);
                };


                $scope.removeRow = function () {
                    $scope.heaterConf.settings.pop();
                };

                $scope.repeatEveryWeek = false;
                $scope.calDateStart = new Date();
                $scope.calDateEnd = new Date();

                $scope.today = function () {
                    $scope.calDateStart = new Date();
                    $scope.calDateEnd = new Date();
                };
                $scope.today();

                $scope.clear = function () {
                    $scope.calDateStart = null;
                    $scope.calDateEnd = null;
                };

                $scope.disabled = function (date, mode) {
                    return '';
                };

                $scope.toggleMin = function () {
                    $scope.minDate = $scope.minDate ? null : new Date();
                };

                $scope.toggleMin();

                $scope.openStart = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.openedS = true;
                    $scope.openedE = false;
                };

                $scope.openEnd = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.openedS = false;
                    $scope.openedE = true;
                };

                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };

                $scope.formats = ['baseDate', 'yyyy-MM-dd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];

                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                var afterTomorrow = new Date();
                afterTomorrow.setDate(tomorrow.getDate() + 2);
                $scope.events = [{
                    date: tomorrow,
                    status: 'full'
                }, {
                        date: afterTomorrow,
                        status: 'partially'
                    }];

                $scope.getDayClass = function (date, mode) {
                    if (mode === 'day') {
                        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                        for (var i = 0; i < $scope.events.length; i++) {
                            var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                            if (dayToCheck === currentDay) {
                                return $scope.events[i].status;
                            }
                        }
                    }

                    return '';
                };

                $scope.saveProfile = function () {
                    $http.post('/profiles/heater/creation', $scope.heaterConf);
                    $scope.shouldShow = "displayAll";

                }

                $scope.cancelProfile = function () {
                    $scope.shouldShow = "displayAll";
                }
            }
        }
    }]);
        