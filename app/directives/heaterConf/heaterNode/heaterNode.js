'use strict';

angular.module('clientApp')
    .directive('heaterNode', ['$http',
        function ($http) {
            return {
                restrict: 'AE',
                scope: {
                    id: "=",
                    days: "=",
                    start: "=",
                    end: "=",
                    avgTemp: "="
                },
                templateUrl: 'directives/heaterConf/heaterNode/heaterNode.html',
                controller: function ($scope) {
                    
                },
                link: function (scope, element, attrs) {
                    console.log(scope.avgTemp);
                    scope.onTempChange = function () {
                        console.log(scope.avgTemp);
                        if (parseInt(scope.avgTemp) < 15){
                            scope.avgTemp = 15;
                        }
                        else if (parseInt(scope.avgTemp) > 30) {
                            scope.avgTemp = 30;
                        }
                    }
                }
            };
        }]);
