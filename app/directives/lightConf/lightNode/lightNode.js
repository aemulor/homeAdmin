'use strict';

angular.module('clientApp')
    .directive('lightNode', ['$http',
        function ($http) {
            return {
                restrict: 'AE',
                replace: false,
                scope: {
                    id: "=",
                    days: "=",
                    start: "=",
                    end: "=",
                    lights: "=",
                    isActive: "="
                },
                templateUrl: 'directives/lightConf/lightNode/lightNode.html',
                controller: function ($scope) {
                    $scope.isActive = 'off';
                    $scope.condition = { light: [''] };
                    $scope.selectChange = function () {
                        for (var j = 0; j < $scope.lights.length; ++j)
                            $scope.lights[j].active = false;
                        for (var i = 0; i < $scope.condition.light.length; ++i) {
                            var res = $scope.condition.light[i].split("_");
                            var floorId = parseInt(res[0]);
                            var roomId = parseInt(res[1]);
                            var relayId = parseInt(res[2]);
                            for (var j = 0; j < $scope.lights.length; ++j) {

                                if ($scope.lights[j].floorId == floorId && $scope.lights[j].roomId == roomId && $scope.lights[j].relayId == relayId) {
                                    if ($scope.isActive == 'on')
                                        $scope.lights[j].active = true;
                                    else
                                        $scope.lights[j].active = false;
                                }

                            }
                        }
                    }
                    $scope.onBtnSwitch = function () {
                        $scope.selectChange();
                    }
                }
            };
        }]);
