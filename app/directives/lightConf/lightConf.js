angular.module('clientApp')
    .directive('lightConf', ['$http', '$compile', function ($http, $compile) {
        return {
            templateUrl: 'directives/lightConf/lightConf.html',
            restrict: 'AE',
            replace: true,
            scope: {
                profile: "=",
                shouldShow: "="
            },
            controller: function ($scope, $http, $compile) {
                var request = $http.get('/dashboard/lights');
                request.success(function (res) {
                    $scope.lights = $scope.lights || res.data;
                });

                $scope.lightConf = {
                    type: "light",
                    name: "",
                    desc: "",
                    startDate: new Date(),
                    endDate: new Date(),
                    settings: []
                };
                
                $scope.$on('editLightProfile', function (event, data) {
                    $scope.editLightProfile(data.prf);
                });


                $scope.editLightProfile = function (profile) {
                    $scope.lightConf = profile;
                    $scope.lightConf.startDate = new Date($scope.lightConf.startDate);
                    $scope.lightConf.endDate = new Date($scope.lightConf.endDate);

                    for (var i = 0; i < $scope.lightConf.settings.lenght; ++i) {
                        $scope.lightConf.settings.start = new Date($scope.lightConf.settings.start);
                        $scope.lightConf.settings.end = new Date($scope.lightConf.settings.end);
                    }
                }

                $scope.addRow = function () {
                    var dummyProfile = {
                        id: $scope.lightConf.settings.length,
                        days: {
                            monday: false,
                            tuesday: true,
                            wednesday: false,
                            thursday: false,
                            friday: false,
                            saturday: false,
                            sunday: false
                        },
                        start: new Date(),
                        end: new Date(),
                        lights: $scope.lights,
                        isActive: false
                    };
                    $scope.lightConf.settings.push(dummyProfile);
                };

                $scope.removeRow = function () {
                    if ($scope.lightConf.settings.lenght > 0)
                        $scope.lightConf.settings.pop();
                };

                $scope.repeatEveryWeek = false;
                $scope.calDateStart = new Date();
                $scope.calDateEnd = new Date();

                $scope.today = function () {
                    $scope.calDateStart = new Date();
                    $scope.calDateEnd = new Date();
                };
                $scope.today();

                $scope.clear = function () {
                    $scope.calDateStart = null;
                    $scope.calDateEnd = null;
                };

                $scope.disabled = function (date, mode) {
                    return '';
                };

                $scope.toggleMin = function () {
                    $scope.minDate = $scope.minDate ? null : new Date();
                };

                $scope.toggleMin();

                $scope.openStart = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.openedS = true;
                    $scope.openedE = false;
                };

                $scope.openEnd = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.openedS = false;
                    $scope.openedE = true;
                };

                $scope.dateOptions = {
                    formatYear: 'yyyy',
                    startingDay: 1
                };

                $scope.formats = ['baseDate', 'yyyy-MM-dd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];

                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                var afterTomorrow = new Date();
                afterTomorrow.setDate(tomorrow.getDate() + 2);
                $scope.events = [{
                    date: tomorrow,
                    status: 'full'
                }, {
                        date: afterTomorrow,
                        status: 'partially'
                    }];

                $scope.getDayClass = function (date, mode) {
                    if (mode === 'day') {
                        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                        for (var i = 0; i < $scope.events.length; i++) {
                            var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                            if (dayToCheck === currentDay) {
                                return $scope.events[i].status;
                            }
                        }
                    }

                    return '';
                };

                $scope.saveProfile = function () {
                    $http.post('/profiles/light/creation', $scope.lightConf);
                    $scope.shouldShow = "displayAll";

                }

                $scope.cancelProfile = function () {
                    $scope.shouldShow = "displayAll";
                }
            }
        }
    }]);
        