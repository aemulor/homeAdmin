angular.module('clientApp')
    .directive('profileCreator', ['$http', '$compile', function ($http, $compile) {
        return {
            templateUrl: 'directives/profileCreator/profileCreator.html',
            restrict: 'AE',
            replace: true,
            scope: {
            },
            controller: function ($scope, $compile) {
                $scope.repeatEveryWeek = false;
                $scope.calDateStart = new Date();
                $scope.calDateEnd = new Date();

                $scope.today = function () {
                    $scope.calDateStart = new Date();
                    $scope.calDateEnd = new Date();
                };
                $scope.today();

                $scope.clear = function () {
                    $scope.calDateStart = null;
                    $scope.calDateEnd = null;
                };

                $scope.disabled = function (date, mode) {
                    return '';
                };

                $scope.toggleMin = function () {
                    $scope.minDate = $scope.minDate ? null : new Date();
                };

                $scope.toggleMin();

                $scope.openStart = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.openedS = true;
                    $scope.openedE = false;
                };

                $scope.openEnd = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.openedS = false;
                    $scope.openedE = true;
                };

                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };

                $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];

                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                var afterTomorrow = new Date();
                afterTomorrow.setDate(tomorrow.getDate() + 2);
                $scope.events = [{
                    date: tomorrow,
                    status: 'full'
                }, {
                        date: afterTomorrow,
                        status: 'partially'
                    }];

                $scope.getDayClass = function (date, mode) {
                    if (mode === 'day') {
                        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                        for (var i = 0; i < $scope.events.length; i++) {
                            var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                            if (dayToCheck === currentDay) {
                                return $scope.events[i].status;
                            }
                        }
                    }

                    return '';
                };                
            }
        }
    }]);