'use strict';

/**
 * @ngdoc directive
 * @name stats
 * @description
 * # adminPosHeader
 */
angular.module('clientApp')
    .directive('stats', function () {
        return {
            templateUrl: 'directives/stats/stats.html',
            restrict: 'E',
            replace: true,
            scope: {
                'profile': '=',
                'fnDetailsAbout': '&'
            },
            controller: function ($scope, $http, $compile) {
                if ($scope.profile.type == 'heater') {
                    $scope.colour = 'danger';
                    $scope.icon = 'fire';
                } else if ($scope.profile.type == 'light') {
                    $scope.colour = 'success';
                    $scope.icon = 'lightbulb-o';
                } else if ($scope.profile.type == 'relay') {
                    $scope.colour = 'warning';
                    $scope.icon = 'bolt';
                }

                // $scope.viewDetails = function () {
                //     if ($scope.profile.type == 'heater') {
                //         $scope.editHeaterProfile($scope.profile);
                //     } else if ($scope.profile.type == 'light') {
                //          $scope.editLightProfile($scope.profile);
                //     } else if ($scope.profile.type == 'relay') {
                //         $scope.editRelayProfile($scope.profile);
                //     }
                // }
            }
            // ,
            // link: function (scope, elm, attrs) {
            //     if (scope.profile.type == 'heater') {
            //         scope.colour = 'danger';
            //         scope.icon = 'fire';
            //     } else if (scope.profile.type == 'light') {
            //         scope.colour = 'success';
            //         scope.icon = 'lightbulb-o';
            //     } else if (scope.profile.type == 'relay') {
            //         scope.colour = 'warning';
            //         scope.icon = 'bolt';
            //     }
            // scope.viewDetails = function () {
            //     console.log("stats: $scope.vieDetails");
            //     console.log(JSON.stringify(scope.profile));
            //     scope.fnDetailsAbout()(scope.profile);
            // }
            // }
        }
    });